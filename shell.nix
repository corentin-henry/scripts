with import <nixos> { };
stdenv.mkDerivation rec {
  name = "flockrs";
  buildInputs = [
    rust-bin.stable."1.41.0".rust
    cargo-edit
    cargo-audit
    cargo-license
  ];
  RUST_BACKTRACE = 1;
  src = null;
}
